import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { SectionComponent } from './section.component';
import { SectionRoutingModule } from './section.routing.module';
import { CommonModule } from '@angular/common';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { EditorModule } from '@tinymce/tinymce-angular';

import { MyDatePickerModule } from 'mydatepicker';




@NgModule({
  imports: [
    SectionRoutingModule,
    ChartsModule,
    FormsModule,
    CommonModule,
    FroalaEditorModule,
    EditorModule,
    MyDatePickerModule
  ],
  declarations: [ SectionComponent ]
})
export class SectionModule { 

}
