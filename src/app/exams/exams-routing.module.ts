import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component'

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Hospital'
    },
    children: [
      {
        path: 'addorview',
        component: CreateComponent,
        data: {
          title: 'add or view'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ExamsRoutingModule {}