import { Component, OnInit } from '@angular/core';
import { adminCommen } from '../../services/admin/adminCommen.service'
import { addAllComponent } from '../../services/addAll.service'
import { ToastrService } from 'toastr-ng2';


import * as moment from 'moment';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [addAllComponent]
})
export class CreateComponent implements OnInit {
  GETHosData;
  constructor( public __Noty:ToastrService,public __ADD:addAllComponent) { }

  ngOnInit() {
    this.GETHOSPITAL();

  }

  SubmitHospital(value){
    if(value){
      this.__ADD.__ADDHOSPITAL(value).subscribe(data => {
      if(data.status){
        this.GETHOSPITAL();

      this.__Noty.info(data.message)
      }else{
      this.__Noty.error(data.message)
      }
      }, err => {
       this.__Noty.error("Somthing went wrong")
      });

    }
  }
  GETHOSPITAL(){
    this.__ADD.__GETHOSpital().subscribe(data => {
      if(data.status=="OK"){
        this.GETHosData=data.data;
        console.log(this.GETHosData,"fdskf");
        }else{
        this.__Noty.error("No data found")
        }
        }, err => {
     this.__Noty.error("Somthing went wrong")
      });
  }

}
