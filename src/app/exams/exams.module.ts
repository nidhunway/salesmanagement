import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { ExamsRoutingModule } from './exams-routing.module';
import { CreateComponent } from './create/create.component';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';



@NgModule({
  imports: [
    ExamsRoutingModule,
    ChartsModule,
    CommonModule,
    FormsModule,
    MyDatePickerModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  declarations: [ CreateComponent ]
})
export class ExamsModule { }
