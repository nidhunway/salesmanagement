import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { environment } from '../../environments/environment';
import { error } from 'util';

@Injectable()

export class addAllComponent {
    headersPost;
    url = environment.backendUrl;
    userData;
    constructor(private http: Http) {
        this.userData=localStorage.getItem('userData');
        console.log(this.userData,'tok');
        

     }
    __ADDHOSPITAL(value) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-access-token', this.userData);

          value.phone.toString()
          console.log(value,"value data");
          
        return this.http.post(`${this.url}/api/admin/add-customer`, value, { headers })
          .map(res => res.json())
          .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }
    addEquipment(value) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-access-token', this.userData);

          
        return this.http.post(`${this.url}/api/admin/add-equipment`, value, { headers })
          .map(res => res.json())
          .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }
    getAllEquipment(){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-access-token', this.userData);
    
       this.headersPost= { headers };
          
        return this.http.get(`${this.url}/api/general/equipments`,this.headersPost)
          .map(res => res.json())
          .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    
    
      }
      __GETHOSpital(){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-access-token', this.userData);
    
       this.headersPost= { headers };
          
        return this.http.get(`${this.url}/api/general/customers`,this.headersPost)
          .map(res => res.json())
          .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    
    
      }

}