import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { environment } from '../../environments/environment';
import { error } from 'util';

@Injectable()

export class QuestionsService {

    url = environment.backendUrl;
    activeUserId: string;
    userDetailsId: string;
    userId: string;
    subjects: Observable<any> = null;
    userData;

    constructor(private http: Http) {

        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.activeUserId = this.userData._id;
        this.userId = this.userData.userId;

    }

    getSubjects() {
        let data = {
            activeUserId: this.activeUserId,
            userId: this.userId
        };

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/getSubjects`, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getMockExams() {
        let data = {
            activeUserId: this.activeUserId
        }

        return this.http.post(`${this.url}/get-mock-tests`, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }

    getAiimsExams() {

        let data = {
            activeUserId: this.activeUserId
        }

        return this.http.post(`${this.url}/get-aiims-exams`, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }
    getDailyExams() {

        let data = {
            activeUserId: this.activeUserId
        }

        return this.http.post(`${this.url}/fetch-daily-exams`, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }

    imageUpload(image) {
        image.append('activeUserId', this.activeUserId)

        let headers = new Headers();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Accept', '*/*');

        let options = new RequestOptions({ headers: headers });

        return this.http.post(`${this.url}/upload-image`, image, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }

    questionUpload(data) {
        data.activeUserId = this.activeUserId;
        return this.http.post(`${this.url}/upload-question`, data)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }
    noteUpload(data) {
        data.activeUserId = this.activeUserId;
        data.userId = this.userId;
        console.log("dataaaSSSSSSSSSSSSaa", data);

        return this.http.post(`${this.url}/create-note`, data)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }
    getAllFaqQuestions() {

        let data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId
        }

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/dynamicFaqGet`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }

    addFaqQstn(data) {
        data.activeUserId = this.activeUserId;
        data.userId = this.userId;
        console.log("dataa", data);


        return this.http.post(`${this.url}/dynamicafaqs`, data)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }
    deleteFaqQuestion(questionId) {
        let data = {
            "activeUserId": this.activeUserId,
            "faqId": questionId,
            "userId": this.userId
        }
        console.log("faqsss", data);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.url}/deletefaqs`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }
    teachersDetails(data) {
        data.activeUserId = this.activeUserId;
        data.userId = this.userId;
        console.log("data", data);

        return this.http.post(`${this.url}/teachersadd`, data)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }
    getAllteachersDetails() {

        let data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId
        }

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/viewallteachers`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }
    getAllNoteDetails() {

        let data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId
        }

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/get-all-mini-note`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }

    getAllAdminNoteDetails() {
        let data = {
            "activeUserId": this.activeUserId,
            "userId": this.userId
        }

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/get-all-mini-note-admin`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }

    deleteTeachersDetails(imageId) {
        let data = {
            "activeUserId": this.activeUserId,
            "imageId": imageId,
            "userId": this.userId
        }
        console.log("faqsss", data);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.url}/deleteteachers`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }
    deleteNoteDetails(questionId) {
        let data = {
            'activeUserId': this.userData._id,
            'editType': 'delete',
            'noteId': questionId,
            'userId': this.userId
        }

        return this.http.post(`${this.url}/edit-mini-note`, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    editNoteDetails(data) {
        data.activeUserId = this.activeUserId;
        data.userId = this.userId;
        data.editType = 'update';

        return this.http.post(`${this.url}/edit-mini-note`, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    mapMockQuestion(mockid, questionid) {
        let data = {
            "activeUserId": this.activeUserId,
            "mockTestId": mockid,
            "questionIds": [questionid]
        }

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/map-question-mock`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }

    getAllMockQuestions(mockid) {

        let data = {
            "activeUserId": this.activeUserId,
            "mockTestId": mockid
        }

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/mock-test-full-questions`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }

    updateQuestion(data) {

        data.activeUserId = this.activeUserId,
            data.editType = 'update'

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/edit-questions`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }

    deleteQuestion(questionId) {
        let data = {
            'activeUserId': this.userData._id,
            'editType': 'delete',
            'questionId': questionId
        }

        return this.http.post(`${this.url}/edit-questions`, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    updateSubjectDetails(data) {

        data.activeUserId = this.activeUserId,
            data.userId = this.userId

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.url}/edit-chapter-section-subject`, data, { headers })
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error'))
    }

}