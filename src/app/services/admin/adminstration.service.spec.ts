import { TestBed, inject } from '@angular/core/testing';

import { AdminstrationService } from './adminstration.service';

describe('AdminstrationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminstrationService]
    });
  });

  it('should be created', inject([AdminstrationService], (service: AdminstrationService) => {
    expect(service).toBeTruthy();
  }));
});
