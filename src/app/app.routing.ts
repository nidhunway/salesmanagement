import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { LoginGuard } from './services/admin/loginGuard';
import { adminLogin } from './commen/login/adminlogin.component';



export const routes: Routes = [
  {
    path: 'admin',
    component: adminLogin
  },
  {
    path: '',
    redirectTo: 'admin',
    pathMatch: 'full'
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'equipment',
        loadChildren: './equipment/equipment.module#EquipmentModule'
      },
      {
        path: 'section',
        loadChildren: './section/section.module#SectionModule'
      },
      {
        path: 'service',
        loadChildren: './engineer/engineer.module#EngineerModule'
      },
      {
        path: 'adduser',
        loadChildren: './adduser/adduser.module#AdduserModule'
      },
      {
        path: 'hospital',
        loadChildren: './exams/exams.module#ExamsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [LoginGuard]
})
export class AppRoutingModule { }
