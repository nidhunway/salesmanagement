import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdduserComponent } from './adduser/adduser.component';
import { NotificationComponent } from '../notification/notification.component';

import { LoginGuard } from '../services/admin/loginGuard';



const routes: Routes = [
  {
    path: '',
    data: {
      title: 'addrole'
    },
    children: [
      {
        path: 'addrole',
        component: AdduserComponent,
        data: {
          title: ''
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [LoginGuard]
})

export class AdduserRoutingModule { }