import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { FormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';

import { MyDatePickerModule } from 'mydatepicker';
import { AdduserRoutingModule } from './adduser-routing.module';

import { AdduserComponent } from './adduser/adduser.component';
import { NotificationComponent } from '../notification/notification.component';


@NgModule({
  imports: [
    AdduserRoutingModule,
    ChartsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    CommonModule,
    FormsModule,
    MyDatePickerModule,
    EditorModule

  ],
  declarations: [
    AdduserComponent,
    NotificationComponent


  ]
})
export class AdduserModule { }
