import { Component } from '@angular/core';
import * as moment from 'moment';
import { addAllComponent } from '../services/addAll.service'
import { ToastrService } from 'toastr-ng2';


// import * as $ from 'jquery';

declare var $: any;

@Component({
  templateUrl: 'equipment.component.html',
  styleUrls: ['equipment.component.css'],
  providers: [addAllComponent]
})

export class EquipmentComponent {
  equipments;
  eqpName;
  constructor(public __eqp:addAllComponent,public __tostr:ToastrService) { }
  ngOnInit() {
    this.getAllExam();
  }
  getAllExam() {
    // this.__eqp.getAllEquipment().subscribe((res) => {
    //   this.equipments = res.data;
    // }, error => {
    //   console.log(error, "error")
    // })
  }
  SpecialSubmit(value) {
    console.log("value", value);
    
    var data = {
      "equipmentName": value.eqp
    }
    this.__eqp.addEquipment(data).subscribe((res) => {
      console.log(res)
      if (res.status) {
        this.__tostr.info(res.message);
       
      } else {
        this.__tostr.error("Somthing went wrong..")

      }
    }, error => {
      this.__tostr.error("Somthing went wrong..")

    })
  }
}