import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { EquipmentComponent } from './equipment.component';
import { EquipmentRoutingModule } from './equipment.routing.module';
import { CommonModule } from '@angular/common';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { EditorModule } from '@tinymce/tinymce-angular';

import { MyDatePickerModule } from 'mydatepicker';




@NgModule({
  imports: [
    EquipmentRoutingModule,
    ChartsModule,
    FormsModule,
    CommonModule,
    FroalaEditorModule,
    EditorModule,
    MyDatePickerModule
  ],
  declarations: [ EquipmentComponent ]
})
export class EquipmentModule { 

}
