import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddQuestionsComponent } from './addview/addview.component';
import { NotificationComponent } from '../notification/notification.component';

import { LoginGuard } from '../services/admin/loginGuard';



const routes: Routes = [
  {
    path: '',
    data: {
      title: 'service'
    },
    children: [
      {
        path: 'engineer',
        component: AddQuestionsComponent,
      
        data: {
          title: 'add or view'
        }
      },
      {
        path: 'notification',
        component: NotificationComponent,
      
        data: {
          title: 'notification'
        }
      }


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [LoginGuard]
})

export class QuestionsRoutingModule { }