import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ViewChild } from '@angular/core';

import { QuestionsService } from '../../services/questions.service';
import { ToastrService } from 'toastr-ng2';
import { adminCommen } from '../../services/admin/adminCommen.service'

// import * as $ from 'jquery';

declare var $: any;

@Component({
  templateUrl: 'addview.component.html',
  styleUrls: ['addview.component.css'],
  providers: [adminCommen,ToastrService]
})

export class AddQuestionsComponent {
  EngineersArry;
  constructor(public __ADD:adminCommen,public __Noty:ToastrService) { }

  ngOnInit() {
  this.GETengineers();
  }
  SubmitEngineer(value){
    var data =value;
    data.role="ENGINEER";
    data.roleId="4"
    this.__ADD.AddEngineer(data).subscribe(data => {
      console.log(data,"see data");
      
      if(data.status){
      this.__Noty.info(data.message)
      this.GETengineers();
      }else{
      this.__Noty.error(data.message)
      }
      }, err => {
        console.log(err,"my errs");

       this.__Noty.error("Somthing went wrong")
      });


}
GETengineers(){
  this.__ADD.GetEngineers().subscribe(data => {
    console.log(data,"see data");
    if(data.status=="OK"){
      this.EngineersArry=data.data;
      console.log(this.EngineersArry);
      

    }else{
      this.__Noty.error("No data found")

    }

    }, err => {

     this.__Noty.error("Somthing went wrong")
    });
}
}
