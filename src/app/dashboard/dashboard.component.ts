import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { adminCommen } from '../services/admin/adminCommen.service'
import { addAllComponent } from '../services/addAll.service'

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css'],
  providers:[adminCommen,addAllComponent]

})
export class DashboardComponent {
  GETEquipmentsData=0;
  GETHosData=0;
  EngineersArry=0;
  constructor(public __ADD:adminCommen,public __ADDNew:addAllComponent) { }
  ngOnInit() {
    this.GETEquipments();
    this.GETHOSPITAL();
    this.GETengineers();
    }
    GETEquipments(){
      // this.__ADDNew.__GETEQUIPMENT().subscribe(data => {
      //   if(data.data) this.GETEquipmentsData=data.data.length;
      //   else this.GETEquipmentsData=0;
        
        // });
    }
    GETHOSPITAL(){
      this.__ADDNew.__GETHOSpital().subscribe(data => {
        if(data.data) this.GETHosData=data.data.length;
        else this.GETHosData=0;
        console.log(this.GETHosData,"data");
        
        });
        }
        GETengineers(){
          this.__ADD.GetEngineers().subscribe(data => {
            if(data.data) this.EngineersArry=data.data.length;
            else this.EngineersArry=0;
            console.log(this.EngineersArry,"data");
            
            });
       
        }
}
